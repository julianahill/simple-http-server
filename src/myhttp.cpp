#include "myhttp.hpp"

/**
 *  
 *
 * @param master
 */
void defaultServer(int master) {
  // Read request from TCP connection and parse it.
  D std::cerr<<"Begin default server"<<std::endl;
  // Accept incoming connections
  struct sockaddr_in clientIPAddress;
  socklen_t len = sizeof(clientIPAddress);
  int slaveSocket = accept(master,
			   (struct sockaddr *) &clientIPAddress,
			   &len);
  if(slaveSocket < 0) {perror("accept"); exit(1);}
  dispatchHTTP(slaveSocket);
    
  // Close TCP connection
  close(slaveSocket);
}

/**
 *  
 *
 * @param master
 */
void forkEach(int master){
  D std::cerr<<"Begin fork server"<<std::endl;
  struct sockaddr_in sockInfo;
  socklen_t len = sizeof(sockInfo);
  int slaveSocket = accept(master,
			   (struct sockaddr*) &sockInfo,
			   &len);
  if(slaveSocket == -1 && errno == EINTR){return;}
  int ret = fork();
  if(!ret){
    dispatchHTTP(slaveSocket);
    exit(0);
  }

  // Close TCP connection
  close(slaveSocket);
}

/**
 *  
 *
 * @param master
 */
void threadEach(int master){
  D std::cerr<<"Begin thread server"<<std::endl;
  struct sockaddr_in sockInfo;
  socklen_t len = sizeof(sockInfo);
  int slaveSocket = accept(master,
			   (struct sockaddr*) &sockInfo,
			   &len);
  if(slaveSocket < 0){perror("accept"); exit(1);}
  // When the thread ends resources are recycled
  std::thread thr (dispatchHTTP, slaveSocket);
  thr.join();
  close(slaveSocket);
}

/**
 *  
 *
 * @param master
 */
void poolThreads(int master){
  D std::cerr<<"Begin pool server"<<std::endl;
  std::thread t1 (loopThread, master);
  std::thread t2 (loopThread, master);
  std::thread t3 (loopThread, master);
  std::thread t4 (loopThread, master);
  std::thread t5 (loopThread, master);
  t1.join(); t2.join(); t3.join(); t4.join(); t5.join();
  for(;true;){}
}

/**
 *  
 *
 * @param master
 */
void loopThread(int masterSocket){
  for (;;) {
    struct sockaddr_in sockInfo;
    socklen_t len = sizeof(sockInfo);
    int slaveSocket = accept(masterSocket,
			     (struct sockaddr*) &sockInfo,
			     &len);
    if(slaveSocket < 0){perror("accept"); exit(-1);}

    dispatchHTTP(slaveSocket);
    close(slaveSocket);
  }
}

/**
 *  
 *
 * @param slave
 */
void dispatchHTTP(int slave){
  std::string docPath = processRequest(slave);

  // Frame the appropriate response header depending on whether
  // the URL requested is found on the server or not.
  if (docPath.compare("false")!=0) {
    // Write the response header to TCP connection.
    // Write requested document (if found) to TCP connection.
    accessPath(slave, docPath);
  } else {
    perror("processRequest");
    writeLevel4(slave, std::string(html), f00, bad_request);
    exit(1);
  }
}


/**
 *  
 *
 * @param slaveSocket
 */ 
std::string processRequest(int slaveSocket){
  std::string collect, documentPath("false");
  // Dup socket fd to input stream file,
  // bc other method wasn't working for me
  D std::cerr<<"slaveSocket="<<slaveSocket<<std::endl;
  int fdinfile = dup(slaveSocket);
  if(fdinfile < 0){perror("dup"); return documentPath;}

  // open dup-ed fd
  FILE * infile = fdopen(fdinfile,"r");
  if(!infile){perror("fdopen"); close(fdinfile); return documentPath;}

  // fsa get get request
  enum{GET, DOC, SPACE}
  state=GET; int gotGet=0;

  D std::cerr<<"request:"<<std::endl;

  for(char c=fgetc(infile); c != EOF; c=fgetc(infile)){
    D std::cerr<<c;
    switch(state){
    case GET:
      if(c==' '){
	if(!collect.compare("GET")) gotGet=1;
	state=DOC; collect.clear();
      }else{collect+=c;}
      break;
    case DOC:
      if(c==' '){state=SPACE; continue;}
      else{collect+=c;}
      break;
    default:
      break;
    }
    if(c=='\n') break;
  }if(gotGet){
    documentPath.clear();
    documentPath+=strdup((char*)collect.c_str());
  }fclose(infile); close(fdinfile); return documentPath;
}


/**
 *  
 *
 * @param write
 * @param path
 */
void accessPath(int write, std::string path){
  if(path.find("/favicon.ico") != std::string::npos) return;
  
  int status; std::string pathFound, docType; 
  
  // map doc path to real file
  if (path.compare("/")==0){
    pathFound+=root; pathFound+=def;
  } else if ((path.find(pilot)!=std::string::npos
	      || path.find(s1)!=std::string::npos
	      || path.find(s2)!=std::string::npos
	      || path.find(s3)!=std::string::npos
	      || path.find(s4)!=std::string::npos
	      || path.find(s5)!=std::string::npos
	      || path.find(s6)!=std::string::npos
	      || path.find(s7)!=std::string::npos)
	      && path.find(at)==std::string::npos) {
     pathFound+=root; pathFound+=at; pathFound+=path; 
  } else if (path.find("/../")!=std::string::npos) {
    pathFound+=root; pathFound+=def;
  } else {
     pathFound+=root; pathFound+=path;
  }

  // get document type
  if(pathFound.find(".html")!=std::string::npos){docType+=html;}
  else if(pathFound.find(".mp4")!=std::string::npos){docType+=mp4;}
  
  // expand filepath and get status
  char * full = NULL;
  char * homeDir = realpath(root, NULL);
  full = realpath(pathFound.c_str(), NULL);
  if(errno == ENOENT && full == NULL){
    free(full);
    free(homeDir);
    D std::cerr<<"404: "<<pathFound<<std::endl;
    writeLevel4(write, std::string(html), f04, file_not_found);
    return;
  }

  if(strlen(full) <= strlen(homeDir)){
     D std::cerr<<"403: "<<full<<std::endl;
     free(homeDir);
     writeLevel4(write, std::string(html), f03, forbidden);
     return;
  } else {status=200; free(homeDir);}

  // set given path to real path and free
  pathFound.clear();
  pathFound+=strdup(full);
  free(full);
  
  DIR * dir=opendir(pathFound.c_str());

  D std::cerr<<"pathFound="<<pathFound.c_str()<<std::endl;
  
  // write correct hearder to socket based
  if(dir) {
    closedir(dir);
    write200(write, std::string(html));
    browseDir(write, pathFound);
  } else if(status==200) {
    write200(write, docType);
    writeDoc(write, pathFound, docType);
  }
}


/**
 *  
 *
 * @param slave
 * @param dir
 */
void browseDir(int slave, std::string dir){
  std::string direc = dir.substr(dir.find_last_of('/')+1);
  
  if(direc.compare(at)==0) {
    std::string t = root;
    t+=def;
    writeDoc(slave, t, std::string(html));
    return;
  }

  DIR * d = opendir((char*)dir.c_str());
  if(d == NULL) {
     perror("opendir");
     exit(-1);
  }
  
  std::string htmlDoc;
  htmlDoc+=htmlBegin; htmlDoc+=direc; htmlDoc+=htmlEndTitle;
  htmlDoc+=direc; htmlDoc+=htmlEndHead; htmlDoc+=htmlLine;
  htmlDoc+=tableEntryHyperLink; htmlDoc+="/../";
  htmlDoc+=tableEntryEndLink; htmlDoc+="/../";
  htmlDoc+=tableEntryEndText;
  
  std::vector<std::string> * files = new std::vector<std::string>();
  for(struct dirent * entry = readdir(d); entry != NULL; entry = readdir(d)) {
     std::string e(strdup((char*)entry->d_name));
     if(e.find(".html")!=std::string::npos) files->push_back(e);
  } closedir(d);
  
  std::sort(files->begin(), files->end());
  
  for(auto && f: *files) {
    std::string tmp_name;
    if(f.find(".html")!=std::string::npos){ 
      int p = f.find(".html");
      for(int i=0; i < p; i++){
	if(f[i] == '-') tmp_name+=' ';
	else if(f[i] == '0' && f[i-1] == '-') continue;
	else tmp_name+=f[i];
      }
    } else {continue;}
    
    htmlDoc+=tableEntryHyperLink;
    htmlDoc+='/'; htmlDoc+=direc; htmlDoc+='/'; htmlDoc+=f;
    htmlDoc+=tableEntryEndLink;
    htmlDoc+=tmp_name; htmlDoc+=tableEntryEndText;
  }
  htmlDoc+=htmlLine; htmlDoc+=htmlEnd;

  int fdout = dup(slave);
  if(fdout < 0){printf("write ok\t"); perror("dup"); return;}
  
  FILE * outFile = fdopen(fdout, "w");
  if(outFile==NULL){
     printf("write ok\t"); 
     perror("fdopen");
     close(fdout);
     return;
  }

  fprintf(outFile, (char*)htmlDoc.c_str());
  fflush(outFile);
  fclose(outFile);
  close(fdout);
}

/**
 *  
 *
 * @param slaveSocket
 * @param documentType
 * @param status
 * @param message
 */
void writeLevel4(int slaveSocket, std::string documentType,
		 const char * status, const char * message){
  std::string collect;
  collect+=http; collect+=sp; collect+=status; collect+=crlf;
  collect+=server; collect+=sp; collect+=s_name; collect+=crlf;
  collect+=ctype; collect+=sp; collect+=documentType; collect+=crlf;
  collect+=crlf;

  int fdout = dup(slaveSocket);
  if(fdout < 0){printf("write lvl 4\t"); perror("dup"); return;}
  
  FILE * outFile = fdopen(fdout, "w");
  if(!outFile){
     printf("write lvl 4\t");
     perror("fdopen");
     close(fdout);
     return;
  }
  
  fprintf(outFile, (char*)collect.c_str());
  fprintf(outFile, message);
  fflush(outFile);
  fclose(outFile);
  close(fdout);
}


/**
 *  
 *
 * @param slaveSocket
 * @param documentType
 */
void write200(int slaveSocket, std::string documentType){
  std::string collect;
  collect+=http; collect+=sp; collect+=ok; collect+=sp;
  collect+="Document"; collect+=sp; collect+="follows"; collect+=crlf;
  collect+=server; collect+=sp; collect+=s_name; collect+=crlf;
  collect+=ctype; collect+=sp; collect+=documentType; collect+=crlf;
  collect+=crlf;

  int fdout = dup(slaveSocket);
  if(fdout < 0){printf("write ok\t"); perror("dup"); return;}

  FILE * outFile = fdopen(fdout, "w");
  if(!outFile){
     printf("write ok\t");
     perror("fdopen");
     close(fdout);
     return;
  }
  
  fprintf(outFile, (char*)collect.c_str());
  fflush(outFile);
  fclose(outFile);
  close(fdout);
}


/**
 *  
 *
 * @param slaveSocket
 * @param doc
 * @param type
 */
void writeDoc(int slaveSocket, std::string doc, std::string type){
   int fdout = dup(slaveSocket); close(slaveSocket);
  if(fdout < 0){printf("write doc\t"); perror("dup"); return;}

  FILE * infile = fopen((char*)doc.c_str(), "r");
  if(infile == NULL){printf("write doc\t"); perror("fopen"); return;}

  fseek(infile, 0, SEEK_END);
  long size = ftell(infile);
  rewind(infile);
  
  char * buff = new char[size];
  size_t s;
  for(s=0; fread(buff+s, 1, 1, infile); ++s);
  buff[s] = '\0'; 
  
  if(s!=write(fdout, buff, s)){
     perror("write");
     fclose(infile);
     delete [] buff;
     close(fdout);
     exit(1);
  }

  fclose(infile);
  delete [] buff;
  close(fdout);
}


/**
 *  
 *
 * @param argc
 * @param argv
 */
int main(int argc, char **argv) {
  D std::cerr<<"Argument Check"<<std::endl;
  // Check if there's enough arguments
  if(argc < 3 && argc != 2) {
    fprintf(stderr, "%s", NOT_ENOUGH_ARGS);
    exit(-1);
  }
  
  // Accept new TCP connection
  int port; char s;
  if(argc==3){s=(char)argv[1][1]; port=atoi(argv[2]);}
  else{s='d'; port=atoi(argv[1]);}
  
  // Check if server type is of a proper value
  if(s!='d' && s!='f' && s!= 't' && s!='p'){
    fprintf(stderr, "%s", NOT_ENOUGH_ARGS); exit(-1);
  } 

  // check if port is allowed
  if(port==80 || (port>1024 && port<65536)) {
     _port=port;
  } else {
     fprintf(stderr, "%s", NOT_ENOUGH_ARGS); exit(-1);
  }
  
  D std::cerr<<"create master attributes"<<std::endl;

  // Set IP address and port for server
  struct sockaddr_in serverIPAddress;
  memset((char*)&serverIPAddress, 0, sizeof(serverIPAddress));
  serverIPAddress.sin_family = AF_INET;
  serverIPAddress.sin_addr.s_addr = INADDR_ANY;
  serverIPAddress.sin_port = htons((u_short)port);
    
  D std::cerr<<"create master"<<std::endl;
  struct protoent * ptrp = getprotobyname("tcp");
  if(!ptrp){perror("getprotobyname"); exit(1);}
  
  // Allocate a socket
  int masterSocket = socket(PF_INET, SOCK_STREAM, ptrp->p_proto);
  if(masterSocket < 0){perror("socket"); exit(1);}

  // Set socket options to reuse port.
  // Otherwise, wait 2 min b4 reuse
  int optval = 1;
  int error = setsockopt(masterSocket, SOL_SOCKET, SO_REUSEADDR,
			 (char*)&optval, sizeof(int));
  if(error < 0){perror("option"); exit(1);}

  // Bind socket to IP address and port
  error = bind(masterSocket, (struct sockaddr *)&serverIPAddress,
	       sizeof(serverIPAddress));
  if(error < 0){perror("bind"); exit(1);}
  
  // Listen and set size of queue of unprocessed connections
  error = listen(masterSocket, QueueLength);
  if(error < 0){perror("listen"); exit(1);}

  D std::cerr<<"server type"<<std::endl;
  // implement server concurrency
  while(1){
    switch(s){
    case 'f':
      forkEach(masterSocket);
      break;
    case 't':
      threadEach(masterSocket);
      break;
    case 'p':
      poolThreads(masterSocket);
      break;
    default:
      defaultServer(masterSocket);
      break;
    }
  }
}
