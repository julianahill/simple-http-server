#ifndef __MYHTTP_HPP__
#define __MYHTTP_HPP__

#include <algorithm>
#include <iostream>
#include <fstream>
#include <thread>
#include <string>
#include <vector>

#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <netdb.h>

#ifdef DEBUG
#define D
#else
#define D for(; false; )
#endif

const char * NOT_ENOUGH_ARGS =
  "MyHTTP needs either: a server type and port number," 
  "\n""or just a port number:"
  "\n"
  "\n""myhttpd [-f | -t | -p] <port>"
  "\n"
  "\n""or"
  "\n"
  "\n""myhttpd <port>"
  "\n"
  "\n"
  "\n""where 1024 < port* < 65536,"
  "\n""-f creates a new process for each request,"
  "\n""-t creates a new thread for each request,"
  "\n""and -p creates a pool of threads."
  "\n"
  "\n"
  "\n""*if you have port 80 setup, then you may"
  "\n"" use port 80 as your port number."
  "\n";

const char * bad_request =
  "<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 5.0//EN\">\n"
  "<html><head>\n"
  "<title>400 Bad Request</title>\n"
  "</head><body>\n"
  "<h1>Bad Request</h1>\n"
  "<p>Your browser did not send a GET request to my server.<br />\n"
  "</p>\n"
  "<hr>\n"                      
  "</body></html>\r\n\r\n";

const char * file_not_found =
  "<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 5.0//EN\">\n"
  "<html><head>\n"
  "<title>404 File Not Found</title>\n"
  "</head><body>\n"
  "<h1>File Not Found</h1>\n"
  "<p>The file you requested cannot be found on the server.<br />\n"
  "</p>\n"
  "<hr>\n"                      
  "</body></html>\r\n\r\n";

const char * forbidden =
  "<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 5.0//EN\">\n"
  "<html><head>\n"
  "<title>404 Forbidden</title>\n"
  "</head><body>\n"
  "<h1>Forbidden</h1>\n"
  "<p>YOU ARE NOT ALLOWED TO ACCESS THIS FILE. GO HOME. YOU ARE DRUNK...<br />\n"
  "</p>\n"
  "<hr>\n"                      
  "</body></html>\r\n\r\n";

/* code that writes an html page to browser a directory  */
const char * htmlBegin = "<!DOCTYPE html><head><title>";
const char * htmlEndTitle = "</title></head><body><h1 style=\"letter-spacing:12px;font-size:15px;position:relative;left:25px;top:10px\">";
const char * htmlEndHead = "</h1><table><tbody>";
const char * htmlLine = "<tr><th colspan=\"5\"><hr></th></tr>";
const char * tableEntryHyperLink = "<tr><td><a style=\"color:#40B3DF;letter-spacing:12px;font-size:15px;\" href=\"";
const char * tableEntryEndLink = "\">";
const char * tableEntryEndText = "</a></td></tr>";
const char * htmlEnd = "</tbody></table></body></html>";

int QueueLength = 5;
int _port;

// basic header strings
const char * sp = " ";
const char * crlf = "\r\n";
const char * crlf2 = "\n\n";
const char * http = "HTTP/1.0";
const char * server = "Server:";
const char * s_name = "myhttp";
const char * ctype = "Content-type:";

// status header response strings
const char * f00 = "400 Bad Request";
const char * f04 = "404 File Not Found";
const char * f03 = "403 Forbidden";
const char * ok = "200 OK";

// content type strings
const char * html = "text/html";
const char * mp4 = "video/mp4";

// document path strings
const char * at = "/at";
const char * pilot = "/pilot";
const char * s1 = "/s1";
const char * s2 = "/s2";
const char * s3 = "/s3";
const char * s4 = "/s4";
const char * s5 = "/s5";
const char * s6 = "/s6";
const char * s7 = "/s7";
const char * root = "/media/ana-gabriel/D866-5411/http-root-dir";
const char * def = "/default.html";

void defaultServer(int master);
void forkEach(int master);
void threadEach(int master);
void poolThreads(int master);
void loopThread(int masterSocket);

void dispatchHTTP(int socket);
std::string processRequest(int socket);
void accessPath(int write, std::string path);

void browseDir(int slave, std::string dir);

void writeLevel4(int slaveSocket, std::string path,
		 const char * status, const char * message);
void write200(int slaveSocket, std::string path);
void writeDoc(int slaveSocket, std::string doc, std::string type);
#endif
