# Simple HTTP Server

This is an implementation of an HTTP server that allows users to watch videos 
from the `http-root-dir` directory. This uses HTML5, so there are some limitations 
as to what can be streamed.  There is a basic html file that will serve as
the default page for the server, but video files must be in either MP4, Ogg, or
WebM (.mp4, .ogg, .webm).

## Installation

```
$ git clone https://gitlab.com/julianahill/simple-http-server.git
$ cd adventure-time-server/src
$ git checkout -b my-branch
$ make
```

## Usage

If you click around in the repo, you'll find the `http-root-dir`, which contains
all of the content for the server. There are a couple of default files in here, 
so you can test it out. It is recommended to remove them, so you can store your own 
videos to stream. To test it, use this command in the terminal:

```
$ ./myhttp <port>
```

In a web browser type:

```
localhost:<port>    
or
<host-ip>:<port>
```
The `host-ip` is the IP address of the computer you are running the server from.
If you don't know what your computer's IP address is, type `ifconfig` in a 
terminal on your machine.  Look for a line with: 
```
inet addr: ... Bcast: ... Mask: ...
```
The string of numbers after `inet addr:`, and, before `Bcast:`, is your computer's
IP address.


If you do chose to add your own content, look at the template html files I have 
in `http-root-dir/at/pilot`.  Don't forget to edit the source code in 
`myhttp.cpp` and `myhttp.hpp` to mirror those changes in `http-root-dir`.